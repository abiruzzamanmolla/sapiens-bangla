# দশ কিস্তি শেষ

---

শেষটা করি মন্টেজুমার গল্প দিয়ে।

মন্টেজুমা ছিলেন আজটেকদের সম্রাট। আজটেকদের শেষ সম্রাট।

শেষ সম্রাট বলেই এই গল্পে আরো একটা চরিত্র চলে আসবে। ইনি গল্পের ভিলেন। শক্তিশালী এই ভিলেনের নাম কর্টেজ।

গল্পের পটভূমি ১৫০০ সাল, আজকের আমেরিকা। আজটেকরা তখন এই আমেরিকায় সবচেয়ে বড় সাম্রাজ্যটির মালিক।

১৪৯২ সালে কলম্বাস যখন তার সোনার তরী নিয়ে ক্যারিবীয় দ্বীপপুঞ্জে এসে হাজির হন, তখন থেকেই সেখানকার আদি বাসিন্দাদের কলোনাইজ করা শুরু করেন। ঐ কলোনীগুলো ছিল পৃথিবীর বুকে এ এক টুকরো দোযখ। বাসিন্দাদের সেখানে খনি আর খেতখামারে দাস হিসেবে খাটানো হত। অমানবিক অত্যাচার আর ইউরোপীয়দের আনা জীবাণুর কবলে পড়ে অতি অল্প সময়েই তারা পটাপট ইহধামের মায়া ত্যাগ করতে লাগলো।

গোটা ক্যারিবীয়ান দ্বীপপুঞ্জ বলতে গেলে জনশূন্য হয়ে গেলো। এই শূন্যস্থান পূরণ করার জন্য তখন আফ্রিকা থেকে দাস আমদানির কালচার শুরু হল। আজ আমরা যে ক্যারিবীয়দের দেখি, তার বড় অংশটাই এই আফ্রিকানদের বংশধর। সহী ক্যারিবীয়ানদের দেখা পাওয়া আজ দুষ্কর।

আজটেকদের দরজার ঐপাশেই ঘটনাগুলো ঘটছিল। যদিও আজটেকরা এর কিছুই জানতো না। ১৫১৯ সালে তাই কর্টেজের নেতৃত্বে স্প্যানিশরা যখন আজটেকদের দরজায় কড়া নাড়ে, তখন বিস্ময়ে মুখ হা হয়ে যাওয়া ছাড়া তাদের আর কিছুই করার ছিল না। আজ যদি পৃথিবীর বুকে এলিয়েন নেমে আসে, তখন আমরা যেমন অবাক হব, আজটেকরাও ইউরোপীয়দের দেখে ঠিক ততোটাই অবাক হয়েছিল।

তাদের সাম্রাজ্যের বাইরেও যে আনুষের অস্তিত্ব আছে---এটা ছিল তাদের ধারণার বাইরে। ইউরোপীয়রা কি মানুষ না অন্য কিছু---এটাই তারা ডিসাইড করে উঠতে পারছিল না। মানুষ হলে এরকম ধবল শাদা হবে কেন? গা থেকে এরকম গন্ধই বা বেরোবে কেন? \(এইখানে বলে রাখি, ইউরোপীয়রা কিন্তু দিনের পর দিন গোসল না করে থাকতে পারতো\)

চেহারা সুরত যাই হোক, ইউরোপীয়দের সাথের জিনিসপত্র দেখে তারা মুগ্ধ না হয়ে পারলো না। এতো বড় বড় জাহাজ তারা বাপের জন্মে দেখেনি। ঘোড়ার মত দ্রুতগামী প্রাণীর সাথেও তাদের পরিচয় সেই প্রথম। সবচেয়ে অবাক করা বিষয়, ইউরোপীয়দের হাতের লাঠি থেকে আগুনের হলকা বেরোয়। কেবল দেবতারাই এরকম লাঠির মালিক হয়ে থাকেন।

\(পাঠকের জন্য ছোট্ট কুইজ। বলতে হবে, এই লাঠিটি আসলে কী?![](https://static.xx.fbcdn.net/images/emoji.php/v9/f9f/1/16/1f61b.png):p\)

নাহ! এরা দেবতা না হয়ে যায় না। হয় দেবতা নয়তো শয়তান।

এখন এরা মানুষ না দেবতা না শয়তান---এটা নিয়ে তর্কবিতর্ক করতে করতেই সময় চলে যেতে লাগলো। এরা যে সাম্রাজ্যের পটেনশিয়াল শত্রু, এদের বিরুদ্ধে যে এখনই সর্বশক্তি নিয়ে ঝাঁপিয়ে পড়া দরকার---এটা তাদের মাথায় এলো না। সাড়ে ৫শ' মানুষের একটা দল এই বিশাল সাম্রাজ্যের আর কী-ইবা ক্ষতি করবে---এই ভেবে তারা নাকে তেল দিয়ে ঘুমাতে লাগলো।

কর্টেজ এর সুবিধাটাই নিলেন। জাহাজরূপী স্পেসশিপ থেকে অবতরণ করে তিনি স্থানীয়দের উদ্দেশ্যে বললেন, স্পেনের রাজা তাকে দূত হিসেবে পাঠিয়েছেন সম্রাট মন্টেজুমার কাছে। মন্টেজুমার সাথে দেখা করতে চায় সে।

ডাহা মিথ্যা কথা। স্পেনের রাজা কস্মিনকালেও মন্টেজুমার নাম শোনেননি। কর্টেজ নিজেই এসব তথ্য যোগাড় করেছে স্থানীয় আজটেক বিরোধী মহলের কাছ থেকে।

এরা তাকে আজটেকদের রাজধানী পর্যন্ত এগিয়ে দিল। কর্টেজ বিনা বাধায় রাজধানীতে প্রবেশ করলেন।

মন্টেজুমার সাথে কথাবার্তা হচ্ছে, এমন সময় কর্টেজ তার সৈন্যদের একটা সিগনাল দিলেন। সঙ্গে সঙ্গে স্প্যানিশরা সম্রাট মন্টেজুমার বডিগার্ডদের কচুকাটা করলো। সম্রাট আর সম্রাট রইলেন না। পরিণত হলেন সম্মানিত বন্দীতে।

সুঁচ হয়ে ঢোকা আর ফাল হয়ে বেরোনো বোধয় একেই বলে। কর্টেজ তবু স্বস্তি পেল না। সম্রাটকে তো বন্দী করলো। কিন্তু তার চারপাশে যে লক্ষ লক্ষ এ্যাজটেক রয়েছে---এদের কী করবে সে? সবাইকে তো আর একবারে ফেলা সম্ভব না এই ৫০০ সৈন্য দিয়ে। কারো কাছে যে সাহায্য চাইবে---সেটাও সম্ভব না। সবচেয়ে কাছের স্প্যানিশ বেসটা রয়েছে এখান থেকে ১৫০০ কিলোমিটার দূরে। কিউবায়।

কর্টেজ করলো কি---সে মন্টেজুমাকে প্রাসাদে বন্দী রেখে দিল। কিন্তু এমন একটা ভাব করলো যেন এখনো মন্টেজুমাই দেশ চালাচ্ছেন। সে জাস্ট বিদেশী গেস্ট। আজটেকদের সমস্যা ছিল---তাদের শাসন ব্যবস্থা ছিল অত্যধিক সেন্ট্রালাইজড। সম্রাটের আদেশই ছিল এখানে শেষ কথা। আর এখন সম্রাটের আদেশ মানে তো দস্যু কর্টেজের আদেশ। আজটেক এলিট সেই আদেশই নতমস্তকে পালন করছিল।

কয়েক মাস এইভাবে চললো. এর মধ্যে চতুর কর্টেজ চারদিকে তার দলবল পাঠিয়ে গোটা সাম্রাজ্য সম্পর্কে একটা পরিষ্কার ধারণা পেয়ে গেলো। স্থানীয় ভাষা, কালচার---সব কিছু সম্পর্কে।

আজটেক এলিটরা যখন শেষমেশ বিদ্রোহ করলো, তখন তারা বিশেষ সুবিধা করে উঠতে আরলো না। কর্টেজ তখন সাম্রাজ্যের ফাঁকফোকরগুলো খুব ভালোমত ইস্তেমাল করা শিখে গেছে। আজটেক শাসকদের বিরুদ্ধে একদল লোক তো খ্যাপা ছিলই। তাদেরকে সে নিজ হাতের মুঠোয় নিয়ে এল।

এই স্থানীয়রা ভেবেছিল, আজটেকদের উৎখাত করে তারা ক্ষমতায় বসে যাবে। স্প্যানিশদের আসল চরিত্র তাদের জানা ছিল না, জানা ছিল না অতি সম্প্রতি ক্যারিবীয় দ্বীপপুঞ্জে তাদের কীর্তিকলাপের কথা। এই অল্প ক'টা স্প্যানিশ যে এক সময় তাদের জন্য বিষফোঁড়া হয়ে দেখা দিবে---এ ছিল তাদের দুঃস্বপ্নের বাইরে।

এর মধ্যে কর্টেজ কিন্তু ইয়াহিয়া খান স্টাইলে আশপাশ থেকে যথেষ্ট সৈন্য এনে মজুদ করে রেখেছে। স্থানীয়রা যখন বুঝলো, কী হচ্ছে---ততোদিনে অনেক দেরি হয়ে গেছে।

মন্টেজুমার গল্প এখানেই শেষ। পরিশিষ্ট হল, ১০০ বছরের মধ্যেই স্থানীয় জনসংখ্যার ৯০ ভাগ নিশ্চিহ্ন হয়ে গেল। ক্যারিবীয়রা যে কারণে মরেছিল, এরাও ঠিক সেই কারণেই মরলো। এক অত্যাচারে। আর বেশিরভাগ মারা পড়লো ইউরোপীয় জীবাণুর বিরুদ্ধে তাদের রোগ প্রতিরোধ ক্ষমতা না থাকায়। আজ যে জীবাণু অস্ত্র কনশাসলি তৈরি হচ্ছে, সেকালে সেই জীবাণু অস্ত্রই আনকনশাসলি একটা জাতিকে প্‌থিবীর বুক থেকে মুছে দিয়েছিল।

হিস্ট্রি রিপিটস ইটসেলফ। দশ বছর পর একই ঘটনা ঘটেলো ইনকাদের সাথে।

জ্ঞান বা জ্ঞানের আগ্রহকে যে গ্লোরিফাই করা হয়---তা তো আর এমনি এমনি না। ইউরোপীয়দের যে জ্ঞান ত্‌ষ্ণাটা ছিল, অজানাকে জানার যে আগ্রহ ছিল, তার ভগ্নাংশও যদি আজটেক বা ইনকাদের থাকতো, তবে আজ হয়তো তারা ইতিহাসের পাতায় না থেকে মর্ত্যের পৃথিবীতেই থাকতো। ক্যারিবীয়দের দুর্দশার কথা জানলে হয়তো আজটেকরা আরো প্রিপেয়ার্ড থাকতো। কিংবা আজটেকদের কথা জানলে ইনকারা। কিন্তু ঐ যে, নিজেদের সীমানাকেই পৃথিবীর সীমানা ভাবা---এই দৃষ্টিভঙ্গিই তাদের সর্বনাশ করলো।

'নলেজ ইজ পাওয়ার', বা আজকের ভাষায় 'ইনফরমেশন ইজ পাওয়ার'---তাই কোন তত্ত্বকথা নয়। ইতিহাস নিজেই এর বড় প্রমাণ।

