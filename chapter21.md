# ছয় কিস্তি শূন্য

---

এইটা কোথ থেকে আসছে জানি না---কিন্তু আমাদের মধ্যে "আমরা ভার্সেস তোমরা" বলে একটা ব্যাপার আছে। এইটা জর্জ বুশ আমাদের মধ্যে ইনজেকশন দিয়া পুশ করে নাই। সুপ্রাচীন কাল থেকেই এটা আমাদের মধ্যে আছে।

আমরা নিজেদের সেইভাবেই ডিফাইন করি---যেটা আমরা না। স্যামুয়েল হান্টিংটন একটা চমৎকার উদাহরণ দিসেন। একজন কর্মজীবী নারী যখন তার দশজন গ্‌হিনী বান্ধবীর সাথে গল্প করেন, তখন তার মাথায় থাকে যে এই গ্রুপে তিনিই একমাত্র কর্মজীবী। সেই নারীই যখন অফিসে তার পুরুষ কলিগদের সাথে মিটিং করেন, তখন কিন্তু তার মাথায় থাকে না যে তিনি একই কোম্পানিতে একই পোস্টে জব করনে। তিনি যে একজন নারী---এই পরিচয়টাই প্রধান হয়ে ওঠে তখন।

চাইলে এরকম হাজারটা উদাহরণ দেয়া যাবে। চিটাং কলেজের যে ছেলেটা ঢাকায় ইঞ্জিনিয়ারিং পড়তে আসে, তার নন-চিটাং বন্ধুদের সামনে হয়তো সে গর্বভরে বলে 'আঁরা চিটাইঙ্গা। আঁরাই বেস্ট'। সেই ছেলেটাই যখন বৈদেশ পড়াশোনা করতে যায়, তখন তার সুর পালটে হয়ঃ "আঁরা বাংলাদেশী। আঁরাই বেস্ট"।

এইভাবেই মানুষ নিজেদের মধ্যে ভাই-বেরাদরসশিপ তৈরি করে। নিজের অস্তিত্বের প্রয়োজনেই তৈরি করে। আর এটা করতে গিয়েই আরেকদল মানুষকে সে অনায়াসে 'তোমরার' দলে ফেলে দেয়। প্রতিটা সভ্যতাই তাই গর্বভরে অন্য সভ্যতার মানুষদের বর্বর ঘোষণা করেছে। মিশরের ফারাওরা অ-মিশরীয় মাত্রকেই বর্বর ভাবতো। রোমানরা ভাবতো অ-রোমানদের। খালি গায়ে ঘোরাফেরা করে বলে ভারতীয়দের বর্বর ভাবতো ব্রিটিশরা। স্বামী-স্ত্রী প্রকাশ্যে চুমু খায় বলে আবার ব্রিটিশ সভ্যতাকে বর্বর ভাবতো ভারতীয়রা।

তবে এই "আমরা বনাম তোমরা" দ্বন্দ্বের মধ্য থেকেও আমাদের মধ্যে কিছু কমন ব্যাপারে বিশ্বাস তৈরি হয়েছে। প্রথম বিশ্বাসটা আসছে টাকায়। তারপর পলিটিক্যাল সিস্টেমে। যে আমাদের কাউকে না কাউকে দরকার আমাদের শাসন করার জন্য। সর্বশেষ আসছে ধর্ম।

ব্যবসায়ী, রাজ-রাজড়া আর ধর্মপ্রচারকরা তাই এই দুনিয়া সবচেয়ে গুরত্বপূর্ণ এলিমেন্ট। গুরুত্বপূর্ণ যদি নাও মানেন, এটা মানতে হবে---এই তিন দল হল সমাজ পরিবর্তনের মূল ফোর্স। আমরা বনাম তোমরা'র যে বাইনারীতে আমরা সাধারণ মানুষ আটকে থাকি, এরা আমাদের সেই বাইনারী থেকে বের করে নিয়ে আসে। ডাক্তার, ইঞ্জিনিয়ার, ক্‌ষক, শ্রমিক---বাদবাকি সবার দরকার সমাজকে ফাংশনাল রাখার জন্য। সমাজ বদলে এরা কোন ভাইটাল ফোর্স না। সমাজে যে র‍্যাডিকাল পরিবর্তনগুলো আসে, তা ঐ বণিক, রাজা আর পয়গম্বরের হাত দিয়েই আসে।

একজন ব্যবসায়ীর কাছে যেমন গোটা দুনিয়াটাই একটা বাজার। আর সব মানুষ তার পটেনশিয়াল কাস্টোমার। কোন বয়বসায়ীকে এইজন্য দেখবেন না তার ব্যবসা নিয়া সন্তুষ্ট থাকতে। দেখা হলেই বলবে, ভাই, আগের মত আর ব্যবসাপাত্তি নাই। খোঁজ নিলে দেখা যাবে, সে তার ব্যবসা বাড়িয়েই চলেছে। ওষুধ থেকে জুতা, জুতা থেকে কসমেটিক্স, কসমেটিক্স থেকে রিয়েল এস্টেট। একই কথা খাটে রাজ-রাজড়াদের জন্যও। গোটা দুনিয়াটাই তার টেন্ট্যাটিভ সাম্রাজ্য। আর দুনিয়ার মানুষ তার পটেনশিয়াল প্রজা। এদিকে ধর্মপ্রচারক মনে করেন, দুনিয়া জুড়ে কেবল একটা সত্যই থাকবে। সবাই দল বেঁধে এই সত্যের অনুসারী হবে।

এই যে গ্লোবাল ভিশন, দুনিয়ার সব মানুষকে এক ফিলোসফির শামিয়ানায় আনার যে চেষ্টা--তাতে কিন্তু দিন শেষে জয়ী হয় টাকাই। পলিটিক্যাল সিস্টেম নিয়া মানুষের মধ্যে মতবিরোধ আছে। গণতন্ত্রই বেস্ট কিনা---এই নিয়া মানুষের মনে এখনো সন্দেহ আছে। অনেককেই বলতে শুনবেন, এর চেয়ে আর্মি ভালো ছিল। কিংবা আগের দিনের জমিদাররাই ভালো ছিল। কিংবা এরশাদের একনায়কতন্ত্রই ভালো ছিল।

ধর্ম নিয়া তো কথাই নাই। আমার গডে হয়তো আপনি বিশ্বাস করেন না। কিংবা আপনার গডে আমি বিশ্বাস করি না, টাকার গডে কিন্তু আমরা সবাই বিশ্বাস করি। আমেরিকান টাকার উপর লেখা থাকেঃ "In God We Trust". এই গড অবশ্যই খ্রিস্টান গড। যে লোকটা টিপিক্যাল গডে বিশ্বাস করে না কিংবা ভিন্ন গডে বিশ্বাস করে, সেও কিন্তু এই টাকা স্বচ্ছন্দে ব্যবহার করে। টাকার উপর গডের কথা লেখা আছে দেখে সে ভারতীয় পণ্য বর্জনের মত এই টাকাও বর্জন করে না। কিংবা আমেরিকান সরকারের কাছে পিটিশনও দাখিল করে না যেঃ তোমার টাকার উপর গডের কথা লেখা আছে। তোমার টাকা আমি আর ইউজ করবো না।

এই একটা স্রোতে ধর্ম-বর্ণ নির্বিশেষে মানুষ এক হয়ে মিশে যায়। টাকাকে আমি-আপনি যতোই গালি দিই না কেন, দুনিয়ার মানুষকে কেউ যদি এক করতে পারে, তবে এই টাকাই।

আমেরিকা ছিল লাদেনের এক নম্বর শত্রু। আমেরিকান ক্‌ষ্টি- কালচার, পলিটিক্স---সব কিছুতেই তার ছিল ভয়ানক বিত্‌ষ্ণা। সেই লাদেনও কিন্তু আমেরিকান ডলারকে প্রিয়তমার মতোই বুকে আগলে রাখতো![](https://static.xx.fbcdn.net/images/emoji.php/v9/f9f/1/16/1f61b.png)

