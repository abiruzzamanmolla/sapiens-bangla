# আট কিস্তি এক

---

জিন মাত্রই স্বার্থপর। সেটা বংশগতির জিনই হোক আর কালচারাল জিনই হোক।

কালচারাল জিনের আবার বলিহারি একটা নাম আছে। ১৯৭৬ সালে রিচার্ড ডকিন্স তার 'The Selfish Gene' বইয়ের শেষ চ্যাপ্টারে এই নতুন প্রকার জিনের সাথে আমাদের পরিচয় করিয়ে দেন। এই জিনের নাম 'Meme'। মেমে নয়, মিমি নয়। মিম। এই জিনের নাম মিম।

সফল জিন যেমন মানষের শরীরকে আশ্রয় করে কাল জয় করে টিকে থাকে, মিমও তেমনি মানুষের মনকে আশ্রয় করে নিজের সাম্রাজ্য বিস্তার করে। মানুষ মরে যায়। জিন ঠিকই তার সন্তানের মধ্য দিয়ে বাহিত হয়। মানুষ মরে গিয়ে ভূত হয়। সফল মিম ঠিকই প্রজন্ম থেকে প্রজন্মে বয়ে চলে।

ডকিন্সখুড়ো আমাদের বলেন, মিম হল একটা কালচারাল ইনফরমেশন। ভাইরাসের মতই এটা আমাদের মনে বাসা বাঁধে। দিনে দিনে আমাদের দেহ মন দুর্বল করে দেয়। আর নিজে শক্তিশালী হয়। ভাইরাস বলেকয়ে আমাদের ম্‌ত্যু পরোয়ানা জারি করে। মিমও অনেক সময় সেটাই করে। তবে আমাদের অবচেতনে। ইসলামী খেলাফত কায়েম হবে কিংবা প্রলেতারিয়েতের রাজত্ব প্রতিষ্ঠা হবে---এই মিমগুলোর কবলে পড়ে আমরা তাই হাসিমুখে ম্‌ত্যুকে ডেকে আনি।

ছোটবেলায় কারো ভাইরাস জ্বর হলে আমরা দিন কয়েক স্কুলে যেতাম না। গেলে যদি সবার মধ্যে ঐ ভাইরাস ছড়িয়ে যায়। মিমের ক্ষেত্রে কিন্তু এই নিয়ম খাটে না। সেলফি ম্যানিয়া যেমন এক রকমের ভাইরাস বা মিম। এখন কেউ সেলফি রোগে আক্রান্ত হলে তার মা নিশ্চয়ই তাকে বলবে না---"দিন দুয়েক স্কুলে যাস না। সবার মধ্যে রোগ ছড়ায়ে দিবি।" বরং সে বিপুল উৎসাহে মাঠে-ঘাটে-লরিতে-বাসে সেলফি-কুলফি-ঈদফি-পূজাফি সব রকমের ভাইরাস ছড়াতে শুরু করে। কেউ তাতে বাধাও দেবে না।

ইন্টারনেটে যে মিমের দেখা আমরা পাই, তা এই ডকিন্সখুড়র মিমেরই সংকীর্ণ ভার্সন। মুশাররফ করিম যখন বলেন, খোদা দড়ি ফালাও, বায়া উঠি যাই কিংবা বন্ধুদের যপনি যখন ফ্রেন্ডস না বলে ফ্রান্স বলে সম্বোধন করেন---এরা প্রত্যেকেই এক এক জন সফল মিম। এদের নিজেদেরে একটা জীবন আছে, জীবনচক্র আছে। কেউ দীর্ঘায়ু, কেউ স্বল্পায়ু।

সমস্যা হচ্ছে, এই মিম থিওরি অনেকে হজম করতে পারেন না। সোশ্যাল ডিনামিক্স ব্যাখ্যা করার জন্য তাদের পছন্দ গেম থিওরি।

গেম থিওরির কী---বোঝার জন্য একটা উদাহরণটা দিই। খুবই পপুলার এগজাম্পল। Prisoners dilemma একে বলে। অনেকেই শুনে থাকবেন। তাও বলি।

ধরেন, আপনাকে আর আপনার এক বন্ধুকে জঙ্গী সন্দেহে পুলিশে ধরে নিয়ে গেল। দুটো আলাদা কক্ষে আপনাদের জেরা করা হল। আপনাদের দু'জনকেই একটা প্যাকেজ অফার করা হল। যদি দু'জনের কেউই দোষ স্বীকার না করেন, তাহলে দু'জনকেই এক বছরের জেল দেয়া হবে। যদি আপনি রাজসাক্ষী হন আর আপনার বন্ধুকে ফাঁসিয়ে দেন, সেই সাথে আপনার বন্ধু চুপচাপ থাকে, তাহলে আপনার সাজা মওকুফ। আর আপনার বন্ধুকে তিন বছরের জেল দেয়া হবে। এবং ভাইস ভার্সা। আর দুইজনই যদি দুইজনকে ফাঁসিয়ে দেন, তাহলে দু'জনেরই দুই বছর করে জেল হবে।

এখন আপনি কী করবেন? বেস্ট হচ্ছে দু'জনই চুপ থাকা। তাহলে দু'জনেরই এক বছর করে জেল হবে। কিন্তু আপনার বন্ধুও যে চুপ থাকবে---তার গ্যারান্টি কী? আপনি চুপ থাকলেন আর আপনার দোস্তো আপনাকে ফাঁসিয়ে দিল---সেক্ষেত্রে তো আপনার তিন বছরের জেল হবে। সেফ থাকার জন্য আপনি আপনার বন্ধুকে ফাঁসিয়ে দিলেন। আপনার বন্ধু বোকাচ্চো হলে আপনার ভাগ্য ভাল। ধেই ধেই করে জেল থেকে বেরিয়ে আসবেন। আর সে তিন বছর জেলে পুড়ে মরবে। কিন্তু সেও যদি আপনাকে ফাঁসিয়ে দেয়! তখন দু'জনই দু'বছর ধরে জেলের ঘানি টানবেন।

খেয়াল করে দেখেন, সেইফ খেলতে গিয়ে আপনি নিজের কিছুটা হলেও ক্ষতি করছেন। গেইম থিওররি মূলকথা এটাই। নিজেদের ক্ষতি হবে জেনেও আমরা এমন কিছু করি, যাতে অন্যেরও অল্প বিস্তর ক্ষতি হয়। নিজের ক্ষতিটুকু তখন আর অতো গায়ে লাগে না।

বাস্তব জীবনটাও এমনই। আপনাকে সবসময়ই এমন দুটো অপশন দেয়া হবে যার একটা খারাপ আর একটা বেশি খারাপ। আপনি তখন মন্দের ভালোটাকেই বেছে নিতে বাধ্য হবেন। বাংলাদেশ বা আমেরিকার নির্বাচনে যেটা বরাবরই হয়ে আসছে। ভালো অপশন বলে দুনিয়ায় কিছু নেই। 'ভালো' একটা মিথ।

অস্ত্রের দৌড়ই ধরি না কেন! এই দৌড়ে কারোরই কোন লাভ হয় না। উলটা দেশ, জনগণ---সবাই পথে বসে। তাও রাষ্ট্র সেইফ থাকার জন্য এই দৌড়ের লোভ সামলাতে পারে না। পাকিস্তান যখন দূরপাল্লার ক্ষেপণাস্ত্র কেনে, তখন ভারতও কিনতে বাধ্য হয়। নাইলে মান সম্মান থাকে না। আবার ভারত যখন পারমাণবিক বোমা বানায়, তখন পাকিস্তানও তার জনগণকে না খাইয়ে না পড়িয়ে হলেও বোমা বানায়। দিন শেষে দু'জনের এ্যাবসোলিউট ক্ষমতা আগের চেয়ে বাড়ে হয়তো, রিলেটিভ ক্ষমতা আগে যা ছিল--তাই থাকে। মাঝখানে অস্ত্রের দৌড়ের জয় হয়। ভাইরাস যেভাবে নিজেকে বিস্তার করে, অস্ত্রের দৌড়ও আমাদের মাথায় কিলবিল কিলবিল করে নিজেকে বিস্তার করে নেয়।

সোজা কথা হচ্ছে,কেউ আমাদের ভাল চায় না। সে গেমই হোক আর মিম-ই হোক। সবাই যার যার ধান্দায় ব্যস্ত। সারাজীবন নিজেদের স্বার্থে আমাদের ইউজ করে কেবল। আর ইউজ শেষ হলে ছুঁড়ে ফেলে দেয়![](https://static.xx.fbcdn.net/images/emoji.php/v9/fe/1/16/1f622.png):'\(

