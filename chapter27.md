# সাত কিস্তি দুই

---

গ্রীক দেব-দেবী বলতে সবাই জিউস, হেরা, এ্যাপোলো---এদের নাম জানি। যেটা জানি না, সেটা হল এদের সবার মাথার উপরে একজন ছিল যাকে এরাও সমঝে চলতো। ইনার নাম আনাখ বা ভাগ্য। প্রায় সকল পলিথেয়িস্টিক ধর্মেই এমন একজন সুপ্রীম সত্তার সন্ধান পাওয়া যায়। পলিথেয়িস্টিক হয়েও এরা তাই বিশুদ্ধ পলিথেয়িস্টিক নয়। এক আর বহু---দু রকম ঈশ্বরে বিশ্বাসই এখানে জড়াজড়ি করে অবস্থান করে।

চাইলে অজস্র উদাহরণ দেয়া যাবে। পশ্চিম আফ্রিকায় ইয়োরুবা নামে একটা ধর্ম আছে। যার বিশ্বাস মতে, সকল দেব-দেবতার জন্ম Olodumare নামের এক সুপ্রীম দেবতার ঔরসে। সবচেয়ে জনপ্রিয় বহু ঈশ্বরবাদী ধর্ম হিন্দু ধর্মেও 'পরমাত্না' নামক এক মহান সত্তার কথা বলা হয়েছে, যে কিনা সমস্ত মানুষ, জীব-জানোয়ার আর অতি অবশ্যই দেব-দেবীদেরও নিয়ন্ত্রণ করে।

পলিথেয়িজমের ঈশ্বরের সাথে ট্রাডিশনাল মনোথেয়িজমের ঈশ্বরের পার্থক্য হচ্ছে---পলিথেয়িজমের ঈশ্বর মানুষের দৈনিক মামলায় নাক গলান না। কার ক্যান্সার হইলো, যুদ্ধে কে মরলো কে বাঁচলো, কে পরীক্ষায় ভালো রেজাল্ট করতে ছায়---এগুলা তার কনসার্নের বিষয় না। মানুষও চালাক। গ্রীকরা তাই আনাখের উদ্দেশ্যে কিছু বলি দিত না কিংবা হিন্দুরাও আত্নার সম্মানে কোন মন্দির বানায় না।

পরম ঈশ্বরের সাথে একমাত্র দেন দরবার হতে পারে যদি তুমি নির্বাণ চাও। হিন্দু সাধুরা যেটা করে। তারা একটা বার্ডস আই ভিউ থেকে জগৎটাকে দেখার অভ্যাস রপ্ত করে। একবার এই অভ্যাস হয়ে গেলে জগতের দুঃখ-দারিদ্র্য, জরা কোন কিছুই তাদের স্পর্শ করে না। তারা এগুলাকে স্বাভাবিক আর ক্ষণস্থায়ী ন্যারেটিভ মেনে নিয়ে পরম ঈশ্বর যেমন এ সব কিছুর প্রতি উদাসীন, তারাও এই উদাসীন এ্যাটিটুডটা গেইন করে।

সাধুরা না হয় বউ-বাচ্চা ছেড়ে দিয়ে পরম ঈশ্বরের সন্ধান পাইলো। আমাদের মত ছাপোষা মানুষের কী হবে? আমরা তো পরম ঈশ্বরের ঐ লেভেলে পৌঁছাতে পারতেসি না। আমাদের ১৪০০ স্কয়ার ফিটের বাসা বা সিভিক হোন্ডার চাহিদা তো ঐ পরম ঈশ্বর মেটাবে না। তো আমাদের জন্য ব্যবস্থা হচ্ছে স্পেশালাইজড ক্ষমতা সম্পন্ন ইন্টারমেডিয়েট দেব-দেবী। লক্ষ্মী আমদের ফ্ল্যাট দিবে, সরস্বতী ডিগ্রি।

বহু ঈশ্বর কিন্তু এক সেন্সে এক ঈশ্বরের চেয়ে ভালো। যে লোক এক ঈশ্বরে বিশ্বাস করে, সে তার বিশ্বাসের কারণেই গোড়া হতে বাধ্য। অন্য ধর্মের দেব-দেবীকে তার ধর্মে কনটেইন করার জায়গা তার নেই। বহু ঈশ্বরবাদীদের এই সমস্যা নাই। এরা অনেক বেশি ওপেন, অনেক বেশি লিবারেল। তার ধর্মে ফ্ল্যাটের দেবী আছে, ডিগ্রির দেবী আছে, কিন্তু ফুটবলের দেবী নাই। এখন অন্য কোন কালচার যদি তার ধর্মে ফুটবলের দেবী আমদানি করে, সে তাতে খুব একটা আপত্তি করবে না। উলটা ওয়েলকাম করবে।

রোমানরা যেমন গ্রীকদের সব দেব-দেবীকে নিজেদের করে নিসে। গ্রীক জিউস রোমে এসে হয়ে গেছে জুপিটার, এরোস হয়ে গেছে কিউপিড আর আফ্রোদিতি, ভেনাস। রোমানরা এই ব্যাপারে এতোটাই উদার ছিল যেঁ এশিয়ান দেবী সিবিল আর মিশরীয় দেবী আইসিসকেও এরা এদের মন্দিরে স্থান দিতে দ্বিধা করেনি।

একমাত্র যে দেবতার সাথে এদের খটোমতো লাগসিলো---সেটা হল খ্রিস্টানদের দেবতা। গড। আল্লাহ। ইয়াওয়েহ। রোমানরা চাচ্ছিলো&lt; খ্রিস্টানরা যেন তাদের আল্লাহর পাশাপাশি জুপিটার আর বেনাসেরও পুজা করে। এটা  
খালি ধর্মীয় লয়্যালটির প্রশ্ন না, রাজনৈতিক রয়্যালটিরও প্রশ্ন।

খ্রিস্টানরা তো কোনভাবেই এক আল্লাহ ছাড়া আর কারও সামনে মাথা নত করবে না। এটাকে যতোটা না ধর্মের বিরুদ্ধে, তার চেয়ে বেশি সাম্রাজ্যের বিরুদ্ধে বিদ্রোহ হিসেবে দেখা হইলো। ফলাফল, যীশুর ম্‌ত্যুর ৩০০ বছরের মধ্যে রাষ্ট্রীয় সন্ত্রাসে কয়েক হাজার খ্রিস্টানের ম্‌ত্যু। মজার ব্যাপার হল, এর পরের ১৫০০ বছর খ্রিস্টানরা নিজেরাই নিজেদের ইচ্চছেমত কচুকাটা করসে। তাও, খুব সিলি একতা ব্যাপার নিয়ে।

খ্রিস্টানদের দুইটা সেক্ট। ক্যাথলিক আর প্রোটেস্ট্যান্ট। দুই দলই বিশ্বাস করে, মরিয়মের ছেলে আমাদের মধ্যে ভালোবাসার বাণী ছড়ায়ে গেসেন। এখন এই ভালোবাসার স্বরূপটা কী---সেটা নিয়ে তাদের মধ্যে দ্বন্দ্ব। প্রোটেস্ট্যান্টরা বিশ্বাস করে, ঈশ্বর স্বয়ং যীশুর বেশে রক্তমাংসে এই প্‌থিবীতে আসছেন, আমাদের জন্য অত্যাচার সহ্য করসেন, শেষমেশ ক্রুশবিদ্ধ হইসেন। আমাদের যেঁ আদি পাপ ছিল, সেটা উনি নিজ কাঁধে নিয়ে নেওয়ায় আমাদের বেহেশতের দরজা আসলে খুলে গেসে। উনার উপর জাস্ট বিশ্বাস স্থাপন করলেই আমরা তরতর করে বেহেশতে চলে যাব। ক্যাথলিকরাও এই মূল প্রিন্সিপালে বিশ্বাস করে। তবে ওদের একটু সংযোজন আছে। এরা মনে করে, বেহেশতে যেতে হলে আমাদের নিয়মিত চার্চে যেতে হবে আর ভালো কাজ করতে হবে। The Big Bang Theory তে বার্নাডেট যখন বলে, সে ক্যাথলিক স্কুলে পড়সে, সে মিথ্যা কথা বলতে পারে না---ঠিক এই কারণেই পারে না। দুনিয়া যেঁ আখিরাতের শস্যক্ষেত্র ---গোঁড়া ক্যাথলিকরা এই ফিলোসফিতে বিশ্বাস করে। প্রোটেস্ট্যান্টরা সেটা করে না।

তাদের কথা হল, ক্যাথলিকরা খোদার সাথে সওদাবাজি করতেসে। ভালো কাজ করলে, আমাকে মান্যিগণ্যি করলে আমাকে প্রমোশন দিবে--এটা অফিসের বসের আচরণ হতে পারে। খোদার না। এতে করে খোদার মহত্বকে খাটো করা হয়। যীশু যে এতো কষ্ট করলো আমাদের জন্য, সেটারও কোন ভ্যালু থাকে না।

এই সামান্য পার্থক্যই ১৬-১৭ শতাব্দীতে হিংস্র রূপ নিলো। ১৫৭২ সালের ২৩শে আগস্ট ফ্রেঞ্চ ক্যাথলিকরা প্রোটেস্ট্যান্টদের উপর নারকীয় হামলা চালায়। ২৪ ঘণ্টার ব্যবধানে ১০,০০০ প্রোটেস্ট্যান্টের লাশ পড়ে যায়। রোমানরা ৩০০ বছরেও এতো খ্রিস্টানের লাশ ফেলতে পারে নাই। রোমের পোপ এই খবর শুনে দুঃখ পাওয়া তো দূরে থাক, খুশিতে ফেটে পড়ে বিরাট ভোজ কাম প্রার্থনার আয়োজন করলেন। ভ্যাটিকানের একটা রুমে এই গণহত্যার ফ্রেস্কো করার জন্য এক পেইন্টারও নিয়োগ দিলেন।

ইতিহাস সাক্ষী দেয়, এই সহনশীলতার অভাবেই মনোথেয়িজম থেকে বার বার 'মুরতাদ' এর জন্ম হয়েছে, পলিথেয়িজম থেকে হয়নি।

